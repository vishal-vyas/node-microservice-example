const mongoose = require('mongoose');

module.exports = Movies = mongoose.model('Movies', new mongoose.Schema({
movieName : {
    type : String,
    required : true
},
releaseYear : {
    type : Number,
    required : true
}
}));

