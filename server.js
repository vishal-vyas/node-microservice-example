const express = require('express')
const app = express();
const routes = require('./Routes/movie-route'); // importing api route
const mongoose = require('mongoose');
const port = process.env.PORT || 3000;

mongoose.connect('mongodb+srv://vishal:vishal@cluster0-x9gih.mongodb.net/test?retryWrites=true&w=majority'
    , { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...',err));

app.use(express.json());
routes(app); // calling get and post methods
app.listen(port, function () {
    console.log(`server started on port ${port}`);
});


