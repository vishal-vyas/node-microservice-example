const controller = require('../controller/controller');

module.exports = function(app) {
    app.route('/movies')
        .get(controller.get_movies);
    app.route('/movies')
        .post(controller.post_movies);
};