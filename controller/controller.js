const Movies = require('../Model/movies-model');

const controllers = {
    get_movies: async function (req, res) {
        await Movies.find((err, data) => {
            if (err) res.status(400).send('Something Wrong');
            res.json(data);
        })
    },
    post_movies: async function (req, res) {
        let movie = new Movies({
            movieName: req.body.moviename,
            releaseYear: req.body.releaseyear
        });
        await movie.save((err, data) => {
            if (err) res.status(400).send('Something Wrong');
            res.send(data);
        });
    },
};

module.exports = controllers;
